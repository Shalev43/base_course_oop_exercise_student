package Abilities;

import Enums.ERocketType;

public class AttackAbility implements IAbility {
    private int amountOfRockets;
    private ERocketType rocketType;

    public AttackAbility(int amountOfRockets, ERocketType rocketType) {
        this.amountOfRockets = amountOfRockets;
        this.rocketType = rocketType;
    }

    public int getAmountOfRockets() {
        return amountOfRockets;
    }

    public void setAmountOfRockets(int amountOfRockets) {
        this.amountOfRockets = amountOfRockets;
    }

    public ERocketType getRocketType() {
        return rocketType;
    }

    public void setRocketType(ERocketType rocketType) {
        this.rocketType = rocketType;
    }
}
