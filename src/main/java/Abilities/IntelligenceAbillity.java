package Abilities;

import Enums.ESensorType;

public class IntelligenceAbillity implements IAbility {
    private ESensorType sensorType;

    public IntelligenceAbillity(ESensorType sensorType) {
        this.sensorType = sensorType;
    }

    public ESensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(ESensorType sensorType) {
        this.sensorType = sensorType;
    }
}
