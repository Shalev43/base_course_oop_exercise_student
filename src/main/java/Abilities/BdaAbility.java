package Abilities;

import Enums.ECameraType;

public class BdaAbility implements IAbility {
    private ECameraType cameraType;

    public BdaAbility(ECameraType cameraType) {
        this.cameraType = cameraType;
    }

    public ECameraType getCameraType() {
        return cameraType;
    }

    public void setCameraType(ECameraType cameraType) {
        this.cameraType = cameraType;
    }
}
