package Enums;

public enum EflightStatus {
    READY_FOR_FLIGHT,
    UNREADY_FOR_FLIGHT,
    ON_FLIGHT;
}
