package Enums;

public enum ECameraType {
    REGULAR,
    THERMAL,
    NIGHT_VISION;
}
