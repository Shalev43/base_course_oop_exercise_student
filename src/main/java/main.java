import Abilities.AttackAbility;
import Abilities.BdaAbility;
import Abilities.IntelligenceAbillity;
import AerialVehicles.Drone.Shoval;
import AerialVehicles.FighterAIrplanes.F15;
import Entities.Coordinates;
import Enums.ECameraType;
import Enums.ERocketType;
import Enums.ESensorType;
import Enums.EflightStatus;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;

public class main {
    public static void main(String[] args) {
        F15 baz = new F15(0, EflightStatus.READY_FOR_FLIGHT, new Coordinates(50.0, 50.5));
        AttackAbility F15AttackAbility = new AttackAbility(2, ERocketType.AMRAM);
        IntelligenceAbillity F15IntelegenceAbility = new IntelligenceAbillity(ESensorType.INFRA_RED);

        baz.getAbilities().add(F15AttackAbility);
        baz.getAbilities().add(F15IntelegenceAbility);

        try {
            AttackMission mission = new AttackMission("gaza", new Coordinates(80.2, 90.6),
                    "shalev", baz);

            IntelligenceMission mission2 = new IntelligenceMission("gaza",
                    new Coordinates(80.2, 90.6),
                    "gal", baz);
            System.out.println("============== Attack mission=========================");
            mission.begin();
            mission.finish();
            System.out.println("============== Intelligence mission=========================");
            mission2.begin();
            mission2.finish();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }

        Shoval shoval = new Shoval(0, EflightStatus.READY_FOR_FLIGHT,
                new Coordinates(888.2, 565.2));
        BdaAbility shovalBdaAbility = new BdaAbility(ECameraType.NIGHT_VISION);
        shoval.addAbility(shovalBdaAbility);

        try {
            BdaMission bdaMission = new BdaMission("street", new Coordinates(555.1,666.2),
                    "david", shoval);
            System.out.println("============== Bda mission=========================");
            bdaMission.begin();
            bdaMission.finish();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }


    }
}
