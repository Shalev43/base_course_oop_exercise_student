package Missions;

import Abilities.IAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

import java.util.HashMap;

public abstract class Mission{
    private Coordinates destination;
    private String pilotName;
    private AerialVehicle executeAerialVehicle;
    private HashMap<String, IAbility> currentAbilities;

    public Mission(Coordinates destination, String pilotName, AerialVehicle executeAerialVehicle) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.executeAerialVehicle = executeAerialVehicle;
        this.currentAbilities = new HashMap<>();
    }

    public void begin(){
        this.executeAerialVehicle.flyTo(this.destination);
        System.out.println("Beginning Mission!");
    }

    public void cancel(){
        this.executeAerialVehicle.land(this.executeAerialVehicle.getAirBaseLocation());
        System.out.println("Abort Mission!");
    }

    public void finish(){
        this.executeMission();
        this.executeAerialVehicle.land(this.executeAerialVehicle.getAirBaseLocation());
        System.out.println("Finish mission!");
    }

    public abstract String executeMission();

    public String getPilotName() {
        return pilotName;
    }

    public AerialVehicle getExecuteAerialVehicle() {
        return this.executeAerialVehicle;
    }

    public Coordinates getDestination() {
        return destination;
    }

    public HashMap<String, IAbility> getCurrentAbilities() {
        return currentAbilities;
    }

    public void addAbility(IAbility abilityToAdd){
        this.currentAbilities.put(abilityToAdd.getClass().getSimpleName(), abilityToAdd);
    }
}
