package Missions;

import Abilities.AttackAbility;
import Abilities.IAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(String target, Coordinates destination, String pilotName, AerialVehicle executeAerialVehicle) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, executeAerialVehicle);
        this.target = target;
        boolean isCompatible = false;
        for (IAbility currentAbility : this.getExecuteAerialVehicle().getAbilities()) {
            if (currentAbility instanceof AttackAbility) {
                isCompatible = true;
                this.addAbility(currentAbility);
            }
        }
        if (!isCompatible) {
            throw new AerialVehicleNotCompatibleException("not compatible");
        }
    }

    @Override
    public String executeMission() {
        AttackAbility attackAbility = (AttackAbility) this.getCurrentAbilities().get("AttackAbility");

        return (this.getPilotName() + " " + this.getExecuteAerialVehicle().getClass().toString() +
                " Attacking suspect " + this.target + " with " + attackAbility.getRocketType().name() +
                "X" + attackAbility.getAmountOfRockets());
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
