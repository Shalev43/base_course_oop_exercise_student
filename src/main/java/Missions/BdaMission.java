package Missions;

import Abilities.BdaAbility;
import Abilities.IAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission {
    private String object;

    public BdaMission(String object, Coordinates destination, String pilotName, AerialVehicle executeAerialVehicle) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, executeAerialVehicle);
        this.object = object;
        boolean isCompatible = false;
        for (IAbility currentAbility : this.getExecuteAerialVehicle().getAbilities()) {
            if (currentAbility instanceof BdaAbility) {
                isCompatible = true;
                this.addAbility(currentAbility);
            }
        }
        if (!isCompatible) {
            throw new AerialVehicleNotCompatibleException("not compatible");
        }
    }

    @Override
    public String executeMission() {
        BdaAbility bdaAbility = (BdaAbility) this.getCurrentAbilities().get("BdaAbility");

        return (this.getPilotName() + " " + this.getExecuteAerialVehicle().getClass().getSimpleName() +
                " tacking pictures of suspect " + this.object + " with " + bdaAbility.getCameraType().name());
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

}
