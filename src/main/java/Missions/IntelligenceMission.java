package Missions;

import Abilities.AttackAbility;
import Abilities.IAbility;
import Abilities.IntelligenceAbillity;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(String region, Coordinates destination, String pilotName, AerialVehicle executeAerialVehicle) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, executeAerialVehicle);
        this.region = region;
        boolean isCompatible = false;
        for (IAbility currentAbility : this.getExecuteAerialVehicle().getAbilities()) {
            if (currentAbility instanceof IntelligenceAbillity) {
                isCompatible = true;
                this.addAbility(currentAbility);
            }
        }
        if (!isCompatible) {
            throw new AerialVehicleNotCompatibleException("not compatible");
        }
    }

    @Override
    public String executeMission() {
        IntelligenceAbillity intelligenceAbillity =
                (IntelligenceAbillity) this.getCurrentAbilities().get("IntelligenceAbillity");

        return (this.getPilotName() + " " + this.getExecuteAerialVehicle().getClass().getSimpleName() +
                " Collectiong Data in Dier al  " + this.region + " with sensor type: "
                + intelligenceAbillity.getSensorType().name());
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}


