package AerialVehicles.FighterAIrplanes;


import Entities.Coordinates;
import Enums.EflightStatus;

public class F16 extends FighterAirplane {
    public F16(int flightHoursFromLastRepair, EflightStatus flightStatus, Coordinates airBaseLocation) {
        super(flightHoursFromLastRepair, flightStatus, airBaseLocation);
    }

}
