package AerialVehicles.FighterAIrplanes;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.EflightStatus;

public abstract class FighterAirplane extends AerialVehicle {
    final static int TIME_TO_REPAIR = 250;

    public FighterAirplane(int flightHoursFromLastRepair, EflightStatus flightStatus,
                           Coordinates airBaseLocation) {
        super(flightHoursFromLastRepair, flightStatus, airBaseLocation, TIME_TO_REPAIR);
    }
}
