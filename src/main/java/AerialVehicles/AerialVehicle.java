package AerialVehicles;


import Abilities.IAbility;
import Entities.Coordinates;
import Enums.EflightStatus;

import java.util.ArrayList;

public abstract class AerialVehicle {
    private int timeToRepair;
    private int flightHoursFromLastRepair;
    private EflightStatus flightStatus;
    private Coordinates airBaseLocation;
    private ArrayList<IAbility> abilities;

    public AerialVehicle(int flightHoursFromLastRepair, EflightStatus flightStatus,
                         Coordinates airBaseLocation, int timeToRepair) {
        this.timeToRepair = timeToRepair;
        this.flightHoursFromLastRepair = flightHoursFromLastRepair;
        this.flightStatus = flightStatus;
        this.airBaseLocation = airBaseLocation;
        this.abilities = new ArrayList<>();
    }

    public void flyTo(Coordinates destination) {
        if (this.flightStatus == EflightStatus.UNREADY_FOR_FLIGHT) {
            System.out.println("Aerial Vehicle isnt ready to fly");
        } else {
            System.out.println("< Flying to " + destination.toString() + " >");
            this.flightStatus = EflightStatus.ON_FLIGHT;
        }
    }

    public void land(Coordinates destination) {
        System.out.println("< landing on " + destination.toString() + " >");
        this.check();
    }

    public void check() {
        if (this.flightHoursFromLastRepair >= this.timeToRepair) {
            this.setFlightStatus(EflightStatus.UNREADY_FOR_FLIGHT);
            this.repair();
        } else {
            this.setFlightStatus(EflightStatus.READY_FOR_FLIGHT);
        }
    }

    public void repair() {
        this.setFlightHoursFromLastRepair(0);
        this.setFlightStatus(EflightStatus.READY_FOR_FLIGHT);
    }

    public ArrayList<IAbility> getAbilities() {
        return abilities;
    }

    public void addAbility(IAbility abilityToAdd) {
        this.abilities.add(abilityToAdd);
    }

    public int getFlightHoursFromLastRepair() {
        return flightHoursFromLastRepair;
    }

    public void setFlightHoursFromLastRepair(int flightHoursFromLastRepair) {
        this.flightHoursFromLastRepair = flightHoursFromLastRepair;
    }

    public EflightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(EflightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Coordinates getAirBaseLocation() {
        return airBaseLocation;
    }

    public void setAirBaseLocation(Coordinates airBaseLocation) {
        this.airBaseLocation = airBaseLocation;
    }

}
