package AerialVehicles.Drone;

import Entities.Coordinates;
import Enums.EflightStatus;

public class Kochav extends Harmes{
    public Kochav(int flightHoursFromLastRepair, EflightStatus flightStatus, Coordinates airBaseLocation) {
        super(flightHoursFromLastRepair, flightStatus, airBaseLocation);
    }
}
