package AerialVehicles.Drone;

import Entities.Coordinates;
import Enums.EflightStatus;

public abstract class Harmes extends Drone {
    final static int TIME_TO_REPAIR = 100;

    public Harmes(int flightHoursFromLastRepair, EflightStatus flightStatus, Coordinates airBaseLocation) {
        super(flightHoursFromLastRepair, flightStatus, airBaseLocation, TIME_TO_REPAIR);
    }
}
