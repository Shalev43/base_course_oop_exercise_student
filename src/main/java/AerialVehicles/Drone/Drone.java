package AerialVehicles.Drone;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.EflightStatus;

public abstract class Drone extends AerialVehicle {
    public Drone(int flightHoursFromLastRepair, EflightStatus flightStatus,
                 Coordinates airBaseLocation, int timeToRepair) {
        super(flightHoursFromLastRepair, flightStatus, airBaseLocation, timeToRepair);
    }

    public String hoverOverLocation(Coordinates destination) {
        this.setFlightStatus(EflightStatus.ON_FLIGHT);
        String hoverStatus = "Hovering over: " + destination.toString();

        System.out.println(hoverStatus);
        return hoverStatus;
    }
}
