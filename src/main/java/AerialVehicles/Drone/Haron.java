package AerialVehicles.Drone;

import Entities.Coordinates;
import Enums.EflightStatus;

public abstract class Haron extends Drone {
    final static int TIME_TO_REPAIR = 150;
    public Haron(int flightHoursFromLastRepair, EflightStatus flightStatus, Coordinates airBaseLocation) {
        super(flightHoursFromLastRepair, flightStatus, airBaseLocation, TIME_TO_REPAIR);
    }
}
