package AerialVehicles.Drone;

import Entities.Coordinates;
import Enums.EflightStatus;

public class Eitan extends Haron {

    public Eitan(int flightHoursFromLastRepair, EflightStatus flightStatus, Coordinates airBaseLocation) {
        super(flightHoursFromLastRepair, flightStatus, airBaseLocation);
    }

}
